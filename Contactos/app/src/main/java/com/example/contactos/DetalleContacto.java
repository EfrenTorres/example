package com.example.contactos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetalleContacto extends AppCompatActivity {
    TextView tvNombre, tvTelefono, tvApellido, tvCorreo;
    Button button;
    String telefono;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_contacto);

        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("NOMBRECONTACTO");
        String apellido = bundle.getString("APELLIDOCONTACTO");
        telefono = bundle.getString("TELEFONOCONTACTO");
        String correo = bundle.getString("CORREOCONTACTO");

        tvNombre = (TextView) findViewById(R.id.tvNombre);
        tvApellido = (TextView) findViewById(R.id.tvApellido);
        tvTelefono = (TextView) findViewById(R.id.tvTelefono);
        tvCorreo = (TextView) findViewById(R.id.tvCorreo);
        button = (Button)findViewById(R.id.btnLlamar);
        tvNombre.setText(nombre);
        tvApellido.setText(apellido);
        tvTelefono.setText(telefono);
        tvCorreo.setText(correo);
    }

    public void llamar(View v) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+telefono));
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(DetalleContacto.this, "NO SE ACTIVO EL PERMISO DE LLAMADAS", Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(intent);
    }
}