package com.example.contactos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lvContactos;
    ArrayList<Contacto> contactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();

        lvContactos = (ListView) findViewById(R.id.lvContactos);
        contactos = new ArrayList<>();
        contactos.add(new Contacto( intent.getStringExtra("nombre"), intent.getStringExtra("apellido"), intent.getStringExtra("telefono"), intent.getStringExtra("correo")));

        ArrayList<String> nombreContactos = new ArrayList<>();
        for (Contacto contacto : contactos) {
            nombreContactos.add(contacto.getNombre() + " " + contacto.getApellido());
        }

        lvContactos.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nombreContactos));

        lvContactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> AdapterView, View view, int position, long l) {
                Intent intent = new Intent(MainActivity.this, DetalleContacto.class);
                intent.putExtra("NOMBRECONTACTO", contactos.get(position).getNombre());
                intent.putExtra("APELLIDOCONTACTO", contactos.get(position).getApellido());
                intent.putExtra("TELEFONOCONTACTO", contactos.get(position).getTelefono());
                intent.putExtra("CORREOCONTACTO", contactos.get(position).getCorreo());
                startActivity(intent);
            }

        });
    }

    /**
     * Agregar un nuevo contacto
     */
    public void agregar(View view) {
        Intent intent = new Intent(this, NuevoContacto.class);
        startActivity(intent);
    }
}