package com.example.contactos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NuevoContacto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_contacto);

        Button agregar = (Button)findViewById(R.id.btnGuardar);

        final EditText nombre = (EditText)findViewById(R.id.txtNombre);
        final EditText apellido = (EditText)findViewById(R.id.txtApellido);
        final EditText telefono = (EditText)findViewById(R.id.txtTelefono);
        final EditText correo = (EditText)findViewById(R.id.txtCorreo);

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);

                intent.putExtra("nombre",nombre.getText().toString());
                intent.putExtra("apellido",apellido.getText().toString());
                intent.putExtra("telefono",telefono.getText().toString());
                intent.putExtra("correo",correo.getText().toString());

                startActivity(intent);
            }
        });
    }
}